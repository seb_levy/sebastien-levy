<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'slevy_wp');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{zZkFv{sR0`~UB^~2u^[oDFA[*dV]&f6}h7XWtLnCCJ2!#^?|2^HkgKk-Z9/@.#(');
define('SECURE_AUTH_KEY',  'D2->}|,E~8& LKAY=lN&)s}1`(4}c6 q{ctdL[,o@0!uCXzc^TO,>r]0){y~Uva~');
define('LOGGED_IN_KEY',    '(+ h69>q_1y$IKK*,ae/pPS~Mx._<9hlz`E>D%@3S#S77{uhOt~?H9<rkcsVi$+N');
define('NONCE_KEY',        '3qqn*UrgMZ#T|iy>iY69QZj:-{l4]]6-*Nw}GQqiMhP=~8Mzu,(M96k5q:_5JXUN');
define('AUTH_SALT',        'N0<3H8mi.[E:(7`gq+ji|)!q[xpo$NaIE8D5j?B1J)BM^72H!Hdf_S*`lYJDUE8Y');
define('SECURE_AUTH_SALT', ':[H!qif-}EV3X|Iv].Y3Ha5AEuwO_2j}+}cv:K#Z,[` $}qJ:RzVM`DQO6a$4qEM');
define('LOGGED_IN_SALT',   'DW-6O.x*}TkR=AMS!;>>r0h8bW1YIq=-DhWad8`isRhfZ|0w9s?07coU,/F^>mo]');
define('NONCE_SALT',       'LB9rUQ+$RQB5$N@UUQ<%r!6ZcbvW!oPtw9>fkL07[moAMcIF|Ccg0@o4+MVH^$9|');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');