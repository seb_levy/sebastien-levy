<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin 
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        
        <meta prefix="og: http://ogp.me/ns#" property="og:image" content="http://www.sebastien-levy.com/wp-content/uploads/LINKEDIN_165x100.jpg" />
        <meta property="og:image" content="http://www.sebastien-levy.com/wp-content/uploads/FACEBOOK_share_H_xlarge.jpg" />
        <meta property="og:image" content="http://www.sebastien-levy.com/wp-content/uploads/FACEBOOK_share_H.jpg" />
        <meta property="og:image" content="http://www.sebastien-levy.com/wp-content/uploads/FACEBOOK_share_292x292.jpg" />
        
        
        <title><?php bloginfo('name'); ?></title>

        <?php wp_head(); ?>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-22446651-1', 'auto');
          ga('send', 'pageview');
        </script>

</head>

<body 
	<?php body_class();	?>
>

<header id="masthead" class="site-header">


    <a href="<?php bloginfo('url'); ?>" class="logo-nav"></a>

    <nav role="navigation" class="single">
        <div class="site-navigation">
                <!-- <?php //custom_language_selector(); ?> -->
                <?php wp_nav_menu( array( 
                                    'theme_location' => 'slevy_theme', 
                                    'menu' => 'single-menu', 
                ) ); // Display the user-defined menu in Appearance > Menus ?>
        </div>
    </nav>      

    <div class="mobile-bar">
        <button class="c-hamburger">          
                <span>toggle menu</span>
        </button>
        <!-- <?php //custom_language_selector(); ?> -->
    </div>
</header><!-- header -->
