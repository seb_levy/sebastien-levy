<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container, begun in the header -->

<footer class="site-footer">
    <div class="overlay"></div>
	<div class="copyright">
		<h3>© 2017 Sebastien Levy</h3>
	</div>
    <div class="site-navigation-footer">
        <!-- <?php //custom_language_selector(); ?> -->
        <?php wp_nav_menu( array( 
                            'theme_location' => 'slevy_theme', 
                            'menu' => 'nav-footer', 
        ) ); // Display the user-defined menu in Appearance > Menus ?>
    </div>
</footer><!-- footer -->

<?php wp_footer(); 
// This fxn allows plugins to insert themselves/scripts/css/files (right here) into the footer of your website. 
// Removing this fxn call will disable all kinds of plugins. 
// Move it if you like, but keep it around.
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/ScrollToPlugin.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/burger-button-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/lib/tiltfx-min.js"></script>

</body>
</html>
