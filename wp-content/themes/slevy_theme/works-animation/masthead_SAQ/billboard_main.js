// Global Variables
var collapseContent_dc, btnCTA_dc, expandedContent_dc,
expExit_dc, btnCloseCTA_dc, endingCTA_dc, replay_dc;

var exitName, numStep, vodkaNum, rhumNum, ginNum, tequilaNum;
var tab = [];

function firstFunction() {
}


function clickCTA() {
    console.info("clickCTA");
}


function transition(numStep) {
    var hide_step = $('.step').eq(numStep - 1 );
    var show_step = $('.step').eq(numStep);

    hide_step.addClass('hide');
    show_step.removeClass('hide');
}

function checkResults() {
    if( vodkaNum === 2 ) {
        document.getElementById("base_resultats").style.backgroundPosition = "0 -1446px";
        // exitName = 'vodka';
        $("#base_resultats").click(function(){
            location.href = "http://www.saq.com/webapp/wcs/stores/servlet/SearchDisplay?utm_source=Multi_Urls_Banner&utm_medium=Banner&utm_content=VODKA_FR&utm_campaign=TrouveTaBase?searchType=&orderBy=1&categoryIdentifier=0510&showOnly=product&langId=-2&beginIndex=0&tri=&metaData=YWRpX2YxOjA8TVRAU1A%2BYWRpX2Y5OiIxIjxNVEBTUD5hZGlfZjk6MQ%3D%3D&pageSize=20&pageView=grid&catalogId=50000&searchTerm=*&sensTri=&facet=adi_f9%3A%221%22%7Cadi_f9%3A%221%22%7Cadi_f9%3A%221%22&storeId=20002";
        });
    }
    else if( rhumNum === 2 ) {
        document.getElementById("base_resultats").style.backgroundPosition = "0 -1190px";
        // exitName = 'rhum';
        $("#base_resultats").click(function(){
                location.href = "http://www.saq.com/webapp/wcs/stores/servlet/SearchDisplay?utm_source=Multi_Urls_Banner&utm_medium=Banner&utm_content=RHUM_FR&utm_campaign=TrouveTaBase?searchType=&orderBy=&categoryIdentifier=0507&showOnly=product&langId=-2&beginIndex=0&tri=&metaData=YWRpX2YxOjA8TVRAU1A%2BYWRpX2Y5OjE%3D&pageSize=20&pageView=&catalogId=50000&searchTerm=*&sensTri=&facet=&storeId=20002";
        });


    }
    else if( tequilaNum === 2 ) {
        document.getElementById("base_resultats").style.backgroundPosition = "0 -882px";
        // exitName = 'tequila';
        $("#base_resultats").click(function(){
                location.href = "http://www.saq.com/webapp/wcs/stores/servlet/SearchDisplay?utm_source=Multi_Urls_Banner&utm_medium=Banner&utm_content=TEQUILA_FR&utm_campaign=TrouveTaBase?searchType=&orderBy=1&categoryIdentifier=0509&showOnly=product&langId=-2&beginIndex=0&tri=&metaData=YWRpX2YxOjA8TVRAU1A%2BYWRpX2Y5OiIxIjxNVEBTUD5hZGlfZjk6MQ%3D%3D&pageSize=20&pageView=grid&catalogId=50000&searchTerm=*&sensTri=&facet=adi_f9%3A%221%22%7Cadi_f9%3A%221%22%7Cadi_f9%3A%221%22&storeId=20002";
        });

    }
    else {
        document.getElementById("base_resultats").style.backgroundPosition = "0 -1702px";
        // exitName = 'gin';
        $("#base_resultats").click(function(){
            location.href = "http://www.saq.com/webapp/wcs/stores/servlet/SearchDisplay?utm_source=Multi_Urls_Banner&utm_medium=Banner&utm_content=GIN_FR&utm_campaign=TrouveTaBase?searchType=&orderBy=1&categoryIdentifier=0505&showOnly=product&langId=-2&beginIndex=0&tri=&metaData=YWRpX2YxOjA8TVRAU1A%2BYWRpX2Y5OiIxIjxNVEBTUD5hZGlfZjk6MQ%3D%3D&pageSize=20&pageView=grid&catalogId=50000&searchTerm=*&sensTri=&facet=adi_f9%3A%221%22%7Cadi_f9%3A%221%22%7Cadi_f9%3A%221%22&storeId=20002";       
        });        
    }
}


function animResults() {
  checkResults();

  $("#base_questionnaire").animate({ top : "-270px" }, 500, 'ease-out' );
  $(".step").find('.img').animate({ top : "-270px" }, 500, 'ease-out' );
  $("#base_resultats").animate ({ top : "0px" }, 500, 'ease-out' );
}



// Add Listener Function
function addListeners() {
    replay_dc.addEventListener('click', resetBanner, false);
}

function resetBanner() {
    numStep = 0;

    vodkaNum    = 0;
    rhumNum     = 0;
    ginNum      = 0;
    tequilaNum  = 0;

    tab = [];

    for (var i = 0; i < $('.step').length; i++) {
        if( i > 0 ) {
            $('.step').eq(i).addClass('hide');
        } else {
            $('.step').eq(i).removeClass('hide');
        }
    }

    $("#base_questionnaire").animate({ top : "0px" }, 300, 'ease-out' );
    $(".step").find('.img').animate({ top : "69px" }, 300, 'ease-out' );
    $("#base_resultats").animate ({ top : "270px" }, 300, 'ease-out' );
}

dcrmInit();

function dcrmInit() {
    // Assign Variables
    replay_dc          = document.getElementById("replay");

    collapseContent_dc = document.getElementById("collapsed");
    endingCTA_dc       = document.getElementById("base_resultats");


    expandedContent_dc = document.getElementById("expanded");
    expExit_dc         = document.getElementById("exit_btn");
    btnCloseCTA_dc     = document.getElementById("close_btn");


    var allImgs = $('.img'); 

    resetBanner();

    // var cosmo         = $('.step').eq(0).find('.left');
    // var rhum          = $('.step').eq(0).find('.right');  
    // var gin           = $('.step').eq(1).find('.left');
    // var soda          = $('.step').eq(1).find('.right');
    // var tequila       = $('.step').eq(2).find('.left');
    // var cuba          = $('.step').eq(2).find('.right'); 
    // var marguarita    = $('.step').eq(3).find('.left');
    // var martini       = $('.step').eq(3).find('.right');

    // Add Listeners
    addListeners();

    // Other
    firstFunction();



allImgs.on('click', function(e) {

    var elementClicked = e.target.classList;

    switch(numStep) {

        case 0:
            numStep++;
            if(elementClicked[1] === "left") {
                vodkaNum++;
                // tab.push("vodka");
                // console.log("étape" + "" + numStep);
                transition(1);
            }

            if(elementClicked[1] === "right") {
                rhumNum++;
                // tab.push("rhum");
                // console.log("étape" + "" +  numStep);
                transition(1);
            }
        break;


        case 1:
            numStep++;
            if(elementClicked[1] === "left") {
                ginNum++;
                // tab.push("gin");
                // console.log("étape" + "" + numStep);
                transition(2);
            }

            if(elementClicked[1] === "right") {
                vodkaNum++;
                // tab.push("vodka");
                // console.log("étape" + "" +  numStep);
                transition(2);
            }
        break;


        case 2:
            numStep++;
            if(elementClicked[1] === "left") {
                tequilaNum++;
                // tab.push("tequila");
                // console.log("étape" + "" + numStep);
                transition(3);
            }

            if(elementClicked[1] === "right") {
                rhumNum++;
                // tab.push("rhum");
                // console.log("étape" + "" +  numStep);
                transition(3);
            }
        break;


        case 3:
            numStep++;
            if(elementClicked[1] === "left") {
                tequilaNum++;
                // tab.push("tequila");
                // console.log("étape" + "" + numStep);
            }

            if(elementClicked[1] === "right") {
                ginNum++;
                // tab.push("gin");
                // console.log("étape" + "" +  numStep);
            }
            // console.log( vodkaNum, ginNum, rhumNum, tequilaNum, Math.max(vodkaNum, ginNum, rhumNum, tequilaNum) );

            animResults();
        break;

        default:
        break;
    }

    // console.log( tab );
});

}
