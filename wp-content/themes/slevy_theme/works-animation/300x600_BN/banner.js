var banner = {};

banner.played                   = 0;
banner.autoplayCount            = 2;
banner.globalSpeed              = 800;
banner.delayBeforeAutoReplay    = 4000;

banner.width = "300px";
banner.height = "600px";

banner.campElements = document.querySelectorAll('.camp');

banner.replay = function() {

    banner.replayBtn.hide();
    banner.endTransition();

    banner.removeInlineStyles();
    banner.resetFlare();
    banner.animation();
};

banner.endTransition = function() {

};

banner.removeInlineStyles = function() {
    // Delete all inline css applied on elements, to re-init before replaying
    var allElements = document.querySelectorAll( 'div, a' );
    for (var i = allElements.length - 1; i >= 0; i--) {
        allElements[i].setAttribute( 'style', '' );
    }
};

banner.resetFlare = function() {
    // Reset flare on cta before replay
    $('.cta-flare').css({  'transition': 'all 0s',
                    '-webkit-transition': 'all 0s',
                    'transform': 'translateX(-50px)',
                    '-webkit-transform': 'translateX(-50px)'
    });
};

banner.checkReplay = function() {
    if ( banner.played < banner.autoplayCount ) {
        setTimeout( banner.replay, banner.delayBeforeAutoReplay );
    } else {
    }
};

banner.resetIntro = function() {

    $('.intro div').attr('style', "");
};



banner.appearCamp = function() {

    for (var i = 0; i < banner.campElements.length; i++) {
        $(banner.campElements[i]).animate( { opacity : 1, scale : 1 }, banner.globalSpeed/5, 'ease-in-out', null, (banner.globalSpeed/5)*i );
    }
};

banner.moveCamp = function() {

    for (var i = 0; i < banner.campElements.length; i++) {
        $(banner.campElements[i]).animate( { translatey : '-62px' }, banner.globalSpeed*0.5, 'ease-in-out');
    }
};


banner.animation = function() {
    
    var timing = 0;

    setTimeout( function() { 
        $('.first-text').animate( { translatex : '0' }, banner.globalSpeed, 'ease-out' );
    }, timing );

    timing+= banner.globalSpeed*2;
    setTimeout( function() {
        $('.first-text').animate( { translatey: '-40px' }, banner.globalSpeed*0.5, 'ease-out' );
        $('.second-text').animate( { translatex : '0' }, banner.globalSpeed, 'ease-out', null, banner.globalSpeed*0.5 );
    }, timing );

    timing+= banner.globalSpeed*4;
    setTimeout( function() {
        $('.first-text').animate( {  opacity : '0' }, banner.globalSpeed, 'ease-out' );
        $('.second-text').animate( {  opacity : '0' }, banner.globalSpeed, 'ease-out' );
        $('.red-bg').animate( { opacity : '1' }, banner.globalSpeed*0.75, 'ease-out', null, banner.globalSpeed*1.25 );
        $('.top-white').animate( { opacity : '1' }, banner.globalSpeed*0.75, 'ease-out', null, banner.globalSpeed*1.25 );
    }, timing );

    timing+= banner.globalSpeed*2;
    setTimeout( function() {
        banner.appearCamp();
    }, timing);


    timing+= banner.globalSpeed*2;
    setTimeout( function() {
        banner.moveCamp();
        $('.third-text').animate( {  translatex : '0' }, banner.globalSpeed, 'ease-out', null, banner.globalSpeed *0.5);
    }, timing);


    timing+= banner.globalSpeed*4;
    setTimeout( function() {
        $('.third-text').animate( {  opacity : 0 }, banner.globalSpeed, 'ease-out' );
        $('.fourth-text').animate( {  translatex : 0 }, banner.globalSpeed, 'ease-out', null, banner.globalSpeed *0.5 );
    }, timing);


    timing+= banner.globalSpeed*2;
    setTimeout( function() {
        $('.cta-button').animate( {  translatex : 0 }, banner.globalSpeed, 'ease-out' );
    }, timing);



    // timing += banner.globalSpeed*2;
    // setTimeout( function() { 
    //     banner.played++;
    //     banner.checkReplay();
    // }, timing );

};

window.onload = function() {
    banner.adContainer  = document.querySelector( '#ad-container' );
    banner.replayBtn    = document.querySelector( '.replay-button' );

    banner.replayBtn.show = function() {
        var replayBtn = this;
        this.classList.remove( 'hide' );
        this.addEventListener( 'click', banner.replay );
    };

    banner.replayBtn.hide = function() {
        var replayBtn = this;
        this.classList.add( 'hide' );
        this.removeEventListener( 'click', banner.replay );
    };

    banner.animation();
};