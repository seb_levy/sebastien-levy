var played,
    adRepeat = 2,
    grouilleMoi;

var glitters = document.querySelectorAll(".single_glitter");
var img = document.querySelectorAll("#animatedGlitters img");


function animeIn(sequence){

    switch(sequence){

        case "seq01":

            animSingleGlittersTitle();

        break;

        case "seq02":

            animSingleGlitters();

            $(".signature_intro").animate({ opacity:0 }, 900, 'ease-in-out', null, 1000 );
            $(".signature_outro").animate({ opacity:1 }, 900, 'ease-in-out', null, 1000 );

            $("#flashlight").animate({ opacity:1, translatey:'80px' }, 1500, 'ease-in-out', function() {
                $("#flashlight").addClass('hide'), 800, 'ease-in', null, 200;               
            }, 800 );

            $(".image_intro").animate({ translateX:'0', translateY:'600px' }, 2800, 'ease-in-out', null, 1000 );
            $("#animatedGlitters").animate({ translatey:'150px' }, 2400, 'ease-out', null, 1000 );

            $(".image_outro").animate({ opacity:1 }, 900, 'ease-in-out', null, 1000 );
            $(".logo_legal").animate({ opacity:1 }, 900, 'ease-in-out', null, 1000 );
            $(".cta").animate({ opacity:1 }, 900, 'ease-in-out', null, 2500 );

            setTimeout(function() {
                grouilleMoi = setInterval( function(){
                    shake();
                }, 1000 );

                $("#animatedGlitters").attr('style', '');
                animSingleGlittersEnd();
            }, 4500);

        break;

        default:
        break;
    }
}

function animSingleGlittersTitle () {
    for (var i = 0; i < 16; i++) {
        $(glitters[i]).stopTranAnim(true, false).attr('style', '').removeClass('hide');
        $(glitters[i]).css({ 'top': Math.random() * (600 - 0) + 0 + "px" } );
        $(glitters[i]).css({ 'left': Math.random() * (300 - 0) + 0 + "px" } );

            $(glitters[i]).animate( { opacity : 1, rotate: 45 }, 70, 'ease-out', function() {
                $(this).addClass('hide');
            }, i*40 );
    }
}


function animSingleGlitters () {
    var delay = 130;
    for (var i = 0; i < glitters.length; i++) {
        $(glitters[i]).stopTranAnim(true, false).css({ 'top': Math.random() * (450 - 150) + 150 + "px" } );
        $(glitters[i]).css({ 'left': Math.random() * (250 - 10) + 10 + "px" } );

            $(glitters[i]).animate( { opacity : 1, rotate: 0 }, 100, 'ease-out', function() {
                $(this).addClass('hide');
            }, i * delay/3);

        if ( delay > 65) {
            delay = delay - 1;

        } else {
            delay = delay + 1;
        }

        console.log( delay );
    }
}


function animSingleGlittersEnd () {
    for (var i = 0; i < 20; i++) {
        $(glitters[i]).stopTranAnim(true, false).attr('style', '').removeClass('hide');
        $(glitters[i]).css({ 'top': Math.random() * (550 - 0) + 0 + "px" } );
        $(glitters[i]).css({ 'left': Math.random() * (230 - 0) + 0 + "px" } );

            $(glitters[i]).animate( { opacity : 1, rotate: 45 }, 500, 'ease-out', function() {
                $(this).addClass('hide');
            }, i*200 );
    }
}


function shake() {

    var ctaRight    = $('.cta.layer:nth-child(3)');
    var ctaLeft     = $('.cta.layer:nth-child(2)');

        ctaRight.animate({ left : '227px'}, function() {
            ctaRight.animate({ left : '217px'}, 200);
        }, 400);

        ctaLeft.animate({ left : '-10px'}, function() {
            ctaLeft.animate({ left : '0'}, 200);
        }, 400 );

}


function animation() {

    setTimeout( function() { animeIn("seq01"); }, 0 );
    setTimeout( function() { animeIn("seq02"); }, 800 );


    // REACTIVATE IT TO STOP THE INFINITE LOOP
    if ( played < adRepeat ) {
        
        setTimeout( replay, 13000 );

    } else if (played >= adRepeat){


        // afficher replay ici
        setTimeout( function(){
            $(".replay_btn").removeClass('hide');

        }, 13000 );

    }

}

function replay() {
    played++;

    clearInterval(grouilleMoi);
   
    $(".replay_btn").addClass('hide');

    // Reinitialise position div pour replay
    setTimeout( function(){

        animation();

        $("#animatedGlitters img").stopTranAnim(true, false).removeClass('hide');
        $("#animatedGlitters img").attr('style', '');
        $(".sprite").attr('style', '');
        $("#flashlight").removeClass('hide');               

    }, 10 );
}

$(function(){
    played = 1;
    animation();
    $(".replay_btn").on('click', replay);

    $('.cta').mouseover( function() {
        console.log('MOUSETEST');
        clearInterval(grouilleMoi);
    } );

    setTimeout( function() {
        clearInterval(grouilleMoi);

    }, 29000);

});



