var played,
    adRepeat = 2;

var replayBtn = $('.replay_btn');
var container = document.querySelector('#adContainer');
var ctaHover = document.querySelector('.cta_hover');
var cta = document.querySelector('.cta');


function animeIn(sequence){

    switch(sequence){

        case "seq01":
            $(".logo").animate({ scale:'1', top : '-90px'  }, 1000, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 0 );   
            $(".sun").animate({ translatey:'380px' }, 1000, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 50 );   
            $(".flower_left").animate({ translatey:'460px', translatex:'-40px', rotate:'0' }, 1000, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 800 );   
            $(".flower_right").animate({ translatey:'477px', translatex:'179px', rotate:'0' }, 1000, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 800 );  
            $(".hands").animate({ translatey:'380px' }, 800, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 600 );   
        break;

        case "seq02":
            $(".logo").animate({ translatey:'-100px'  }, 600, 'ease-in-out', null, 0 );   
            $(".tag").animate({ scale:'1', opacity : 1 }, 800, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 300 );   
        break;

        case "seq03":
            $(".tag").animate({ opacity : 0 }, 800, 'ease-in-out', null, 0 );   
            $(".logo").animate({ scale:'0.75', top : '-430px' }, 1000, 'ease-in-out', null, 800 );   
            $(".date").animate({ scale:'1' }, 400, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 1500 );   
            $(".cta").animate({ scale:'1' }, 400, 'cubic-bezier(0.75, 0.5, 0.3, 1)', null, 1800 );   
            $(".cta.text").animate({ opacity : 1 }, 400, 'ease-in-out', null, 2000 );   

            setTimeout(function() {
                        container.addEventListener("mouseover", rollOver );
                        container.addEventListener("mouseout", rollOut );
            }, 2500);
        break;

        default:
        break;
    }
}


function animation() {

    setTimeout( function() { animeIn("seq01"); }, 0 );
    setTimeout( function() { animeIn("seq02"); }, 2000 );
    setTimeout( function() { animeIn("seq03"); }, 5000 );

// REACTIVATE IT TO STOP THE INFINITE LOOP
    if ( played < adRepeat ) {
        setTimeout( replay, 10000 );

    } else if (played >= adRepeat){
  
        setTimeout( function(){
             // afficher replay ici
            replayBtn.animate({ opacity:'1' }, 250, 'ease-in-out', null, 0 ); 
            replayBtn.on('click', setReplay);  
        }, 10000 );
    }
}




function rollOver() {
    $(ctaHover).animate({ opacity : 1 }, 200 );
    $(cta).animate({ opacity : 0 }, 100 );
}
function rollOut() {
    $(ctaHover).animate({ opacity : 0 }, 200 );
    $(cta).animate({ opacity : 1 }, 100 );
}



function setReplay(){
        // effacer replay ici
        replayBtn.animate({ opacity:'0' }, 0, 'ease-in-out', null, 0 ); 
        replayBtn.off('click', setReplay);
        replay();
}

function replay() {
        played++;

        // Reinitialise position div pour replay
        setTimeout( function(){
            animation();

            container.removeEventListener("mouseover", rollOver );
            container.removeEventListener("mouseout", rollOut );
            
            $("div").attr('style', '');  
        }, 500 );
}


$(function(){
        played = 1;
        animation();
});



