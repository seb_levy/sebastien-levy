var banner = {};
var element;

banner.played                   = 0;
banner.autoplayCount            = 1;
banner.globalSpeed              = 500;
banner.delayBeforeAutoReplay    = 4000;



banner.replay = function() {

    banner.replayBtn.hide();
    banner.endTransition();

    banner.removeInlineStyles();
    banner.animation();
};

banner.endTransition = function() {
    // Insert animation transition before replaying the banner here, if needed
    // End animation transition
    $('.drape').removeClass('collapsed');
    $('.cta-container').removeClass('pulse');
};

banner.removeInlineStyles = function() {
    // Delete all inline css applied on elements, to re-init before replaying
    var allElements = document.querySelectorAll( 'div, a' );
    for (var i = allElements.length - 1; i >= 0; i--) {
        allElements[i].removeAttribute('style');
    }
};

banner.checkReplay = function() {
    if ( banner.played < banner.autoplayCount ) {
        setTimeout( banner.replay, banner.delayBeforeAutoReplay );
    } else {
        banner.replayBtn.show();
    }
};


function animSpritesheet ( element, hStep, vStep, spriteMaxFrame, timesToPlay, hideAfter ) {

    var spriteCurrFrame = 0,
        timesPlayed     = 0;

    element = $('.smoke');

    var anim = setInterval( function() {

        if ( timesPlayed < timesToPlay ) {
            if ( spriteCurrFrame < spriteMaxFrame ) {
                element.css( 'background-position', '-' + ( hStep * spriteCurrFrame ) + 'px' + ' ' + '-' + ( vStep * spriteCurrFrame) + 'px' );
                spriteCurrFrame++;
            } else {
                element.css( 'background-position', '-' + ( hStep * (spriteCurrFrame-1) ) + 'px' + ' ' + '-' + ( vStep * (spriteMaxFrame-1) ) + 'px' );
                spriteCurrFrame = 0;
                timesPlayed++;
            }
        } else {
            clearInterval( anim );
            if( !!hideAfter ) {
                element.animate({ opacity: 0 }, banner.globalSpeed, 'ease-in-out' );
            }
        }

    }, 100);

}




banner.animation = function() {
    var timing = 0;

    setTimeout( function() {
        $('.item.un').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.deux').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
    }, timing);
    
    timing += banner.globalSpeed*2;
    setTimeout( function() {
        $('.item.trois').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.quatre').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)', null, banner.globalSpeed*0.5 );
        $('.item.six').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)', null, banner.globalSpeed*0.75 );
        $('.item.cinq').animate({ translate : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)', null, banner.globalSpeed*1 );
    }, timing);

    
    timing += banner.globalSpeed*4;
    setTimeout( function() {
        $('.item.deux').animate({ translatey : '-68px', scale:0.75}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.un').animate({ opacity : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.trois').animate({ opacity : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.quatre').animate({ opacity : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.six').animate({ opacity : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );
        $('.item.cinq').animate({ opacity : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)' );

        $('.text.un').animate({ opacity : 1}, banner.globalSpeed, 'ease-in-out', null, banner.globalSpeed );
        $('.text.deux').animate({ opacity : 1}, banner.globalSpeed, 'ease-in-out', null, banner.globalSpeed*3 );
    }, timing);

    
    timing += banner.globalSpeed*6;
    setTimeout( function() {
        $('.logo').animate({ opacity : 0}, banner.globalSpeed, 'ease-in-out' );

        $('.item.deux').animate({ opacity : 0}, banner.globalSpeed, 'ease-in-out' );
        $('.text.un').animate({ opacity : 0}, banner.globalSpeed, 'ease-in-out' );
        $('.text.deux').animate({ opacity : 0}, banner.globalSpeed, 'ease-in-out' );

        $('.machine').animate({ translatex : 0}, banner.globalSpeed, 'cubic-bezier(0,1,.67,.98)', null, banner.globalSpeed );
        $('.mask').animate({ translatey : '-50px'}, banner.globalSpeed, 'linear', null, banner.globalSpeed*2 );
        $('.cafe').animate({ translatey : '50px'}, banner.globalSpeed, 'linear', null, banner.globalSpeed*2 );
        $('.jusdpipi').animate({ height : '100%'}, banner.globalSpeed, 'linear', function(){
            $(this).animate({ height : 0 }, banner.globalSpeed, 'linear' );
            $('.smoke').animate({ opacity : 1 }, banner.globalSpeed/3, 'linear' );
            animSpritesheet ( element, 95, 0, 19, 3, true );
            // $('.smoke').animate({ opacity : 1 }, banner.globalSpeed, 'linear' );
        }, banner.globalSpeed*1.75 );

        $('.button.off').animate({ scale : 1}, banner.globalSpeed, ' cubic-bezier(.87,-.41,.19,2)', function() {
            $('.button.on').animate({ opacity : 1}, banner.globalSpeed, 'ease-in-out' );

        }, banner.globalSpeed*1.75 );
        $('.text.end').animate({ opacity : 1}, banner.globalSpeed, ' ease-in-out', null, banner.globalSpeed*1.75 );
        $('.text.arrow').animate({ opacity : 1}, banner.globalSpeed, ' ease-in-out', null, banner.globalSpeed*1.75 );
    }, timing);


    timing += banner.globalSpeed*2;
    setTimeout( function() { 
        banner.played++;
        banner.checkReplay();
    }, timing );

};






window.onload = function() {
    banner.adContainer  = document.querySelector( '#ad-container' );
    banner.replayBtn    = document.querySelector( '.replay-button' );

    banner.btn          = document.querySelector( '.button.on' );

    banner.replayBtn.show = function() {
        var replayBtn = this;
        replayBtn.classList.remove( 'hide' );
        replayBtn.addEventListener( 'click', banner.replay );
    };

    banner.replayBtn.hide = function() {
        var replayBtn = this;
        replayBtn.classList.add( 'hide' );
        replayBtn.removeEventListener( 'click', banner.replay );
    };

    banner.btn.addEventListener('mouseover', function() {
        $(this).addClass('scaleBtn');
    })

    banner.btn.addEventListener('mouseout', function() {
        $(this).removeClass('scaleBtn');
    })


    banner.animation();
};