var banner = {};
var element;

banner.played                   = 0;
banner.autoplayCount            = 1;
banner.globalSpeed              = 800;
banner.delayBeforeAutoReplay    = 4000;


banner.glitchVstep = 300;
banner.glitchHstep = 0;


banner.replay = function() {

    banner.replayBtn.hide();
    banner.endTransition();

    banner.removeInlineStyles();
    banner.animation();
};

banner.endTransition = function() {
    // Insert animation transition before replaying the banner here, if needed
    // End animation transition
    $('#end-frame-hover').css('pointerEvents', 'none');
};

banner.removeInlineStyles = function() {
    // Delete all inline css applied on elements, to re-init before replaying
    var allElements = document.querySelectorAll( 'div, a' );
    for (var i = allElements.length - 1; i >= 0; i--) {
        allElements[i].setAttribute( 'style', '' );
    }
};

banner.checkReplay = function() {
    if ( banner.played < banner.autoplayCount ) {
        setTimeout( banner.replay, banner.delayBeforeAutoReplay );
    } else {
        banner.replayBtn.show();
    }
};

banner.resetIntro = function() {

    $('.intro div').attr('style', "");
};





function animSpritesheet ( element, speed, hStep, vStep, spriteMaxFrame, timesToPlay, hideAfter ) {

    var spriteCurrFrame = 0,
        timesPlayed     = 0;

    element = $(element);

    var anim = setInterval( function() {

        if ( timesPlayed < timesToPlay ) {
            if ( spriteCurrFrame < spriteMaxFrame ) {
                element.css( 'background-position', '-' + ( hStep * spriteCurrFrame ) + 'px' + ' ' + '-' + ( vStep * spriteCurrFrame) + 'px' );
                spriteCurrFrame++;
            } else {
                element.css( 'background-position', '-' + ( hStep * (spriteCurrFrame-1) ) + 'px' + ' ' + '-' + ( vStep * (spriteMaxFrame-1) ) + 'px' );
                spriteCurrFrame = 0;
                timesPlayed++;
            }
        } else {
            clearInterval( anim );
            if( !!hideAfter ) {
                element.animate({ opacity: 0 }, banner.globalSpeed*0, 'ease-in-out' );
            }
        }
    }, speed);
}




banner.animation = function() {
    
    var timing = banner.globalSpeed;

    setTimeout( function() {
        animSpritesheet ( '.bg-glitch', 100 , banner.glitchVstep, banner.glitchHstep, 5, 3, true );
    }, timing);


    timing+=banner.globalSpeed*1.5;
    setTimeout( function() {
        $('.frame-dot-black').animate({ opacity : 1}, 0 );
        animSpritesheet ( '.frame-dot-black', 100 , banner.glitchVstep, banner.glitchHstep, 3, 3, true );
    }, timing);


    timing+=banner.globalSpeed/2;
    setTimeout( function() {
        $('.dedsek-pink').animate({ opacity : 1}, 0 );
        animSpritesheet ( '.dedsek-pink', 100 , banner.glitchVstep, banner.glitchHstep, 3, 3, true );
    }, timing);


    timing+=banner.globalSpeed;
    setTimeout( function() {
        $('.frame-waves-black').animate({ opacity : 1}, 0 );
        animSpritesheet ( '.frame-waves-black', 100 , banner.glitchVstep, banner.glitchHstep, 4, 4, true );
    }, timing);


    // timing+=banner.globalSpeed*1.5;
    // setTimeout( function() {
    //     $('.lightning').animate({ opacity : 1}, 0 );
    //     animSpritesheet ( '.lightning', banner.glitchVstep, banner.glitchHstep, 3, 1, true );
    // }, timing);


    timing+=banner.globalSpeed;
    setTimeout( function() {
        $('.dedsek-pink').animate({ opacity : 1}, 0 );
        $('.line-1').addClass('animText');
    }, timing);

    
    timing+=banner.globalSpeed;
    setTimeout( function() {
        $('.line-2').addClass('animText');
    }, timing);


    timing+=banner.globalSpeed;
    setTimeout( function() {
        $('.line-3').addClass('animText');
    }, timing);


    timing+=banner.globalSpeed*1.5;
    setTimeout( function() {
        $('.dedsek-pink').animate({ opacity : 0}, 0 );
        $('.line-1').removeClass('animText');
        $('.line-2').removeClass('animText');
        $('.line-3').removeClass('animText');

        $('.frame-dot-black').animate({ opacity : 1}, 0 );
        $('.skull-pink').animate({ opacity : 1}, 0 );
        animSpritesheet ( '.skull-pink', 100 , banner.glitchVstep, banner.glitchHstep, 6, 1, true );
    }, timing);

    timing+=banner.globalSpeed;
    setTimeout( function() {
        $('.frame-dot-black').animate({ opacity : 0}, 0 );
        $('.end-codelines').animate({ opacity : 1}, 0 );
        $('.end-frame').animate({ opacity : 1}, 0 );
        $('.end-copy').animate({ opacity : 1}, 0 );
        $('.end-repear').animate({ opacity : 1}, 0 );
        animSpritesheet ( '.end-repear', 100 , banner.glitchVstep, banner.glitchHstep, 6, 5, false );
        animSpritesheet ( '.end-codelines', 500 , banner.glitchVstep, banner.glitchHstep, 6, 1, false );
        
        $('#end-frame-hover').css('pointerEvents', 'all');

    }, timing);


    timing += banner.globalSpeed*2;
    setTimeout( function() { 
        banner.played++;
        banner.checkReplay();
    }, timing );

};

window.onload = function() {
    banner.adContainer  = document.querySelector( '#ad-container' );
    banner.hover        = document.querySelector( '.end-frame-hover' );
    banner.replayBtn    = document.querySelector( '.replay-button' );

    banner.replayBtn.show = function() {
        var replayBtn = this;
        replayBtn.classList.remove( 'hide' );
        replayBtn.addEventListener( 'click', banner.replay );
    };

    banner.replayBtn.hide = function() {
        var replayBtn = this;
        replayBtn.classList.add( 'hide' );
        replayBtn.removeEventListener( 'click', banner.replay );
    };

    banner.animation();
};