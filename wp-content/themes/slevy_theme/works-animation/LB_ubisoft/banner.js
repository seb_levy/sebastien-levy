var played,blobsContainer,
    adRepeat    = 2,
    replayBtn   = $('.replay_btn'),
    globalSpeed = 500;

var adWidth     = 728,
    adHeight    = 90;

// var blobsContainer  = document.querySelector( '#blobsContainer' );


// // create some blobs
// var blobAmount  = 200;
// var blobs       = [];
// for (var i = 0; i < blobAmount; i++) {
//     var blob    = document.createElement('div');
 
//     blobsContainer.appendChild(blob);
//     blobs.push(blob);
// }

// function randomRange(e, t) {
//     return e + Math.random() * (t - e);
// }

// function blobFalling( delay ) {
//     var tempLines = blobsContainer.querySelectorAll('div');

//     for (var i = 0; i < tempLines.length; i++) {
//         var rotation = randomRange(0, 180);
//         // var decalage = randomRange(-20, 20);

//         var departX = randomRange(-adWidth/2, adWidth*0.75);
//         var departY = randomRange(-10, -20);

//         var destX = randomRange(adWidth/4, adWidth*1.5);
//         var destY = randomRange(adHeight+10, adHeight+20);

//         var radius  = randomRange(1, 3);
//         var opacity = randomRange(0.1, 0.3);

//         tempLines[i].style.backgroundColor  = 'rgba(255, 100, 80, 1)';
//         tempLines[i].style.borderColor      = 'rgba(0, 0, 0, 0.2)';
//         tempLines[i].style.borderStyle      = 'solid';
//         tempLines[i].style.borderWidth      = randomRange(1, 2)+'px';
//         tempLines[i].style.width            = radius + 'px';
//         tempLines[i].style.height           = radius*2 + 'px';
//         tempLines[i].style.borderRadius     = '100%';
//         tempLines[i].style.left             = '-5px';
//         tempLines[i].style.top              = '0px';
//         tempLines[i].style.opacity          = opacity;


//         // console.log( decalage );
//         $(tempLines[i]).css( '-webkit-transform', 'translate3d( '+departX+'px, '+departY+'px, 0 ) rotate( '+rotation+'deg )' );
//         $(tempLines[i]).css( '-moz-transform', 'translate3d( '+departX+'px, '+departY+'px, 0 ) rotate( '+rotation+'deg )' );
//         $(tempLines[i]).css( '-ms-transform', 'translate3d( '+departX+'px, '+departY+'px, 0 ) rotate( '+rotation+'deg )' );
//         $(tempLines[i]).css( '-o-transform', 'translate3d( '+departX+'px, '+departY+'px, 0 ) rotate( '+rotation+'deg )' );
//         $(tempLines[i]).css( 'transform', 'translate3d( '+departX+'px, '+departY+'px, 0 ) rotate( '+rotation+'deg )' );

//         $(tempLines[i]).animate({ opacity: 1, translate3d: destX+'px, '+destY+'px, 0' }, globalSpeed*4, 'linear', null, (delay) * i );
//     }
// }






function animeIn(sequence) {

    switch(sequence) {

         case "seq01":
            $(".logo").animate({ opacity: 0 }, 2000, 'ease-in-out', null, 500 );
            $(".apex").animate({ translatey: '-460px' }, 4500, 'ease-in-out', null, 2000 );
            $(".bg_intro").animate({ translateY: 0, translateX: '-560px', translateY:'-10px', opacity : 0 }, 3000, 'ease-in-out', null, 800 );
        break;


        case "seq02":
            $(".bg_cave").animate({ opacity: 1}, 200, 'ease-in-out', null, 0 );
            $(".logo_cave").animate({ opacity: 1}, 500, 'ease-in-out', null, 200 );
            $(".copy1").animate({ opacity: 1}, 500, 'ease-in-out', null, 200 );
            $(".copy2").animate({ opacity: 1}, 500, 'ease-in-out', null, 200 );
        break;

        case "seq03":
            $(".copy1").animate({ opacity: 0}, 500, 'ease-in-out', null, 200 );
            $(".ctaContainer").animate({ opacity: 1}, 500, 'ease-in-out', null, 200 );
        break;
 
        default:
        break;

    }

}



function animation() {

    // blobFalling( 150 );

    setTimeout( function() { animeIn("seq01"); }, 0 );
    setTimeout( function() { animeIn("seq02"); }, 8000 );
    setTimeout( function() { animeIn("seq03"); }, 10000 );




    // REACTIVATE IT TO STOP THE INFINITE LOOP
    if ( played < adRepeat ) {
        setTimeout( replay, 15000 );

    } else if (played >= adRepeat) {
  
        setTimeout( function() {
            // afficher replay ici
            replayBtn.animate({ opacity:'1' }, 250, 'ease-in-out', null, 0 );
            replayBtn.on('click', setReplay);
        }, 15000 );
    }

}


function setReplay() {
    // effacer replay ici
    replayBtn.animate({ opacity:'0' }, 0, 'ease-in-out', null, 0 ); 
    replayBtn.off('click', setReplay);
    replay();
}


function replay() {
    played++;

    // Reinitialise position div pour replay
    setTimeout( function() {
       $("div, a").attr('style', '');
       animation();
    }, 0 );
}


$(function() {
    played = 1;
    animation();
});