var played,
    replayBtn,
    adRepeat    = 1,
    globalSpeed = 500;

var adWidth     = 300,
    adHeight    = 600;



/////////////////////
// F U N C T I O N S



function animation() {

    var timing = 0;

    setTimeout( function() {
        $('.copytext1').animate({ scale : 1 }, globalSpeed, 'cubic-bezier(0.75, 0.5, 0.3, 1.2)' );
        $('.copytext2').animate({ translatey : '10px' }, globalSpeed, 'cubic-bezier(0.75, 0.5, 0.3, 1.9)', null, globalSpeed*1.5 );
        $('.copytext3').animate({ translatey : '4px' }, globalSpeed, 'cubic-bezier(0.75, 0.5, 0.3, 1.9)', function() {
            $('.gif').css( 'display', 'block');
        }, globalSpeed*2 );

        // LINES TOP
        // $('.containerLines1').animate({ scale : 1, translatey : '-20px' }, globalSpeed/2, 'ease-in', function(){
        //     $(this).css('transformOrigin', 'top center');
        //     $(this).animate({ opacity : 0, scale : 1.2, translatey : '-30px' }, globalSpeed/3, 'ease-out' );
        // }, globalSpeed*0.5 );

        // // LINES BOTTOM
        // $('.containerLines2').animate({ scale : 1, translatey : '20px' }, globalSpeed/2, 'ease-in', function(){
        //     $(this).css('transformOrigin', 'top center');
        //     $(this).animate({ opacity : 0, scale : 1.2, translatey : '30px' }, globalSpeed/3, 'ease-out' );
        // }, globalSpeed*3.5 );

    }, timing );

    timing+= globalSpeed*8;
    setTimeout( function() {
        $('.gif').animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in' );
        $('.containerText3').animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', null, globalSpeed*0.5 );
        $('.containerText2').animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', null, globalSpeed*1 );
        $('.containerText1').animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', null, globalSpeed*1.5 );

        // LINES TOP
        $('.containerLines1 .line1').animate({ translatex : '-12px', translatey : '-12px', rotate : '45deg' }, globalSpeed, 'ease-in-out' );
        $('.containerLines1 .line2').animate({ translatey : '-22px' }, globalSpeed, 'ease-in-out' );
        $('.containerLines1 .line3').animate({ translatex : '19px', translatey : '-12px', rotate : '-45deg' }, globalSpeed, 'ease-in-out' );
        
        // LINES BOTTOM
        $('.containerLines2 .line1').animate({ translatex : '-12px', translatey : '12px', rotate : '45deg' }, globalSpeed, 'ease-in-out', null, globalSpeed*2 );
        $('.containerLines2 .line2').animate({ translatey : '22px' }, globalSpeed, 'ease-in-out', null, globalSpeed*2 );
        $('.containerLines2 .line3').animate({ translatex : '19px', translatey : '12px', rotate : '-45deg' }, globalSpeed, 'ease-in-out', null, globalSpeed*2 );

        $('.containerLines1').animate({ opacity : 0 }, globalSpeed/2, 'linear', null, globalSpeed*1.5);
        $('.containerLines2').animate({ opacity : 0 }, globalSpeed/2, 'linear', null, globalSpeed*3);
    }, timing );

    timing+= globalSpeed*3;
    setTimeout( function() {
        $('.scannerV').animate({ translatex : 0 }, globalSpeed*1.5, 'ease-in-out' );
        $('.containerText4').animate({ width : adWidth + 'px' }, globalSpeed*1.5, 'ease-in-out', function() {
            $('.containerText5').css('opacity', '1');
        } );
    }, timing );


    timing+= globalSpeed*3.5;
    setTimeout( function() {
        $('.scannerH').animate({ translatex : 0 }, globalSpeed*1.5, 'ease-in-out' );
    }, timing );

    timing+= globalSpeed*3;
    setTimeout( function() {

        $('.scannerH').animate({ translatey : '-236px' }, globalSpeed*1.5, 'ease-in-out', function() {
            $(this).animate({ opacity : 0 }, globalSpeed/2 );
        } );

        // $('.containerText5').animate({ height : "167px" }, globalSpeed*1.5, 'ease-in-out', function() {
            // $('.containerText5').css('opacity', '1');
        // } );      

        $('.containerText4').animate({ height : 0 }, globalSpeed*1.5, 'ease-in-out');

    }, timing );


    timing+= globalSpeed*6;
    setTimeout( function() {
        $('.copytext5').eq(3).animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in' );
        $('.copytext5').eq(2).animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', null, globalSpeed*0.5 );
        $('.copytext5').eq(1).animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', null, globalSpeed*1 );
        $('.copytext5').eq(0).animate({ translatex : -adWidth + 'px' }, globalSpeed, 'ease-in', function() {
            $('.mask').css( 'display','block');
            $('.containerText6').css( 'display','block');
            $('.mask').animate({ border : '0px solid #3c3c3b'  }, globalSpeed, 'ease-in' );
            $('.corner').eq(0).animate({ translatex : '-100px', translatey : '-100px', opacity : 1 }, globalSpeed, 'ease-in' );
            $('.corner').eq(1).animate({ translatex : '-100px', translatey : '100px', opacity : 1 }, globalSpeed, 'ease-in' );
            $('.corner').eq(2).animate({ translatex : '100px', translatey : '-100px', opacity : 1 }, globalSpeed, 'ease-in' );
            $('.corner').eq(3).animate({ translatex : '100px', translatey : '100px', opacity : 1 }, globalSpeed, 'ease-in' );
        }, globalSpeed*1.5 );
    }, timing );

    timing+= globalSpeed*6;
    setTimeout( function() {
        $('.mask').animate({ border : '110px solid #3c3c3b'  }, globalSpeed, 'ease-in' );
        $('.corner').eq(0).animate({ translatex : '0', translatey : '0', opacity : 0 }, globalSpeed, 'ease-in' );
        $('.corner').eq(1).animate({ translatex : '0', translatey : '0', opacity : 0 }, globalSpeed, 'ease-in' );
        $('.corner').eq(2).animate({ translatex : '0', translatey : '0', opacity : 0 }, globalSpeed, 'ease-in' );
        $('.corner').eq(3).animate({ translatex : '0', translatey : '0', opacity : 0 }, globalSpeed, 'ease-in', function() {
            $('.mask').css( 'display','none');
            $('.containerText6').css( 'display','none');
        });
        $('#adContainer').animate({ backgroundColor : '#7b003b' }, globalSpeed, 'ease-in', function() {
            $('.containerLines2 .line1').attr('style', '');
            $('.containerLines2 .line2').attr('style', '');
            $('.containerLines2 .line3').attr('style', '');
            $('.containerLines2 .line1').css('backgroundColor', '#7b003b');
            $('.containerLines2 .line2').css('backgroundColor', '#7b003b');
            $('.containerLines2 .line3').css('backgroundColor', '#7b003b');
            $('.containerLines2').attr('style', '');
        }, globalSpeed*1 );

    }, timing );

    timing+= globalSpeed*2;
    setTimeout( function() {
        $('.outro').animate({ opacity : 1, translatey : 0  }, globalSpeed, 'ease-in', function(){
            $('.gif_end').css( 'display', 'block');
        } );
        $('.legal').animate({ opacity : 1  }, globalSpeed, 'ease-in', function() {
            $('.ctaContainer').animate({ opacity : '1' }, globalSpeed );
        }, globalSpeed );
    }, timing );

}





///////////////
// R E P L A Y

function replay() {
    played++;
    replayBtn.off('click', replay);
    replayBtn.addClass('hide');

    // Insert animation transition before replaying the banner here, if needed
    // End animation transition //

    // Delete all inline css applied on elements by ZeptoJS, to re-init before replaying
    setTimeout( function() {
        $("div, a").attr('style', '');
        animation();
    }, 100 );
}

function checkReplay() {
    if ( played < adRepeat ) {
        setTimeout( replay, 3000 );
    } else {
        replayBtn.removeClass('hide');
        replayBtn.on('click', replay);
    }
}





window.onload = function() {

    replayBtn   = $( '.replay_btn' );
    played = 1;
    animation();

};