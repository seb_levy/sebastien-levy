var played,
    replayBtn,
    letter, arrowWithText, logoContainer, logo, gif, crochet, copytext, cta, hitzone, shadow,
    adWidth, adHeight,
    adRepeat    = 1,
    globalSpeed = 500;

/////////////////////
// F U N C T I O N S

function animation() {

    var timing = 0;
   
    timing+=globalSpeed*7;
    setTimeout( function() {
        gif.animate({ translatex : '-285px' }, globalSpeed*1.5, 'ease-in-out');
    }, timing );  

    timing+=globalSpeed*0;
    setTimeout( function() {
        letter.animate({ translatey : '0' }, globalSpeed, 'ease-in-out');
        shadow.animate({ opacity : 1 }, globalSpeed, 'ease-in-out', null, globalSpeed*0.5);
        crochet.animate({ translatey : '0' }, globalSpeed, 'ease-in-out', function() {
            $(this).animate({ translatey : '-113px' }, globalSpeed*2, 'ease-in-out' );
            copytext.animate({ opacity : 1 }, globalSpeed*2, 'ease-in-out');
        });
    }, timing );

    timing+=globalSpeed*6;

    setTimeout( function() {
        // slide left
        logoContainer.animate({translatex: '-100%'}, globalSpeed*2, 'ease-in-out', function() {
            // position to right of frame
            $(this).css({transform: 'translateX(100%)'});
            //shift children to the left
            logoContainer.children().css({'transform': 'translateX(-171px)'});
        });
        arrowWithText.animate({ translatex: '0%' }, globalSpeed*2, 'ease-in-out');
    }, timing );

    timing+=globalSpeed*6;

    setTimeout( function() {
        arrowWithText.animate({ translatex: (-adWidth+207)+"px" }, globalSpeed*2, 'ease-in-out');
        logoContainer.animate({ translatex : '0%' }, globalSpeed*2, 'ease-in-out');
    }, timing );

    timing+=globalSpeed*2;

    setTimeout( function() {
        $(cta).animate({ opacity : 1 }, globalSpeed*2, 'ease-in-out', function() {
            // checkReplay();
        });
    }, timing);
}

///////////////
// R E P L A Y

function replay() {
    played++;
    replayBtn.off('click', replay);
    replayBtn.addClass('hide');

    // Insert animation transition before replaying the banner here, if needed
    // End animation transition //

    // Delete all inline css applied on elements by ZeptoJS, to re-init before replaying
    setTimeout( function() {
        $("div, a").attr('style', '');
        // flipText1.removeClass('textFlipOut');
        animation();
    }, 100 );
}

function checkReplay() {
    if ( played < adRepeat ) {
        setTimeout( replay, 3000 );
    } else {
        replayBtn.removeClass('hide');
        replayBtn.on('click', replay);
    }
}

window.onload = function() {
    cta         = document.querySelector('.ctaContainer');
    hitzone     = document.querySelector('#hitzone');
    
    replayBtn   = $( '.replay_btn' );

    letter      = $( '.letter' );       
    shadow      = $( '.shadow' );       
    crochet     = $( '.crochet' );
    logoContainer = $( '.logo-container' );
    logo        = $( '.logo' );       
    gif         = $( '.human_gif' );
    copytext    = $( '.copytext' );
    arrowWithText = $( '.arrow-with-text' );

    adWidth = 728;
    adHeight = 90;


    played = 1;
    animation();
};