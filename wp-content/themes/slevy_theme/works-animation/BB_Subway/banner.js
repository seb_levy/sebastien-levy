var played, allDIV, elements,
    adRepeat = 2;

var replayBtn   = $('.btn_replay');


// REMOVE CLASS ANIMATION //
    function removeClass(target, animClass){
        allDIV.removeClass('textFlip');
        allDIV.removeClass('textSlideDown');
        allDIV.removeClass('textSlideRight');
        allDIV.removeClass('textSlideLeft');
    }


// SET CLASS ANIMATION //
    function setTextAnimClass(target, animClass){

        target.addClass(animClass);
    }

// SHOW ELEMENT  //
    function show_flip (numFrame, numLine, animClass) {
        var tempLines = $('.frame' + numFrame).eq(numLine);
            setTextAnimClass(tempLines, animClass);
    }

    function show_slide (numFrame, numLine, animClass) {
        var tempLines = $('.frame' + numFrame).eq(numLine);
            setTextAnimClass(tempLines, animClass);
    }

// HIDE ELEMENT //
    function hide(type, productNum) {
        var tempLines = $('.' + type).eq(productNum);
            tempLines.animate({ opacity: '0' }, 0, 'ease-out', null );
    }



function animeIn(sequence){

    switch(sequence){

        case "seq01":
            $(".frame1").eq(0).removeClass('textFlip').animate( { rotateX: '0deg', translateX: '0px' }, 0);
            $(".frame1").eq(0).animate({ translatex : '-300px', rotateX:'0deg' }, 500, 'ease-in-out', null, 0 ); 

            $(".frame1").eq(1).removeClass('textFlip').animate( { rotateX: '0deg', translateX: '0px' }, 0);
            $(".frame1").eq(1).animate({ translatex : '-300px', rotateX:'0deg' }, 500, 'ease-in-out', null, 0 ); 

            $(".conteneurFrame1").animate({ translatex : '-300px' }, 500, 'ease-in-out', null, 0 ); 
        break;


        case "seq02":
            $(".frame2").eq(0).removeClass('textFlip').animate( { rotateX: '0deg', translateX: '0px' }, 0);
            $(".frame2").eq(0).animate({ translatex : '300px', rotateX:'0deg' }, 500, 'ease-in-out', null, 0 ); 

            $(".frame2").eq(2).removeClass('textFlip').animate( { rotateX: '0deg', translateX: '0px' }, 0);
            $(".frame2").eq(2).animate({ translatex : '300px', rotateX:'0deg' }, 500, 'ease-in-out', null, 0 ); 

            $(".conteneurFrame2").animate({ translatex : '300px' }, 500, 'ease-in-out', null, 0 ); 
        break;

        case "seq03":
            $(".RolloverCTA").animate( { opacity: '1' }, 0);
        break;

        default:
        break;
    }
}


function animation() {

    setTimeout( function() {             
        $(".logo").animate({ scale : '1' }, 500, 'ease-in-out', null, 0 ); 
    }, 0 );

    setTimeout( function(){ show_flip(1, 0, 'textFlip', 800); }, 1000  );
    setTimeout( function(){ show_flip(1, 1, 'textFlip', 800); }, 1200  );
    setTimeout( function(){ show_slide(1, 2, 'textSlideDown', 800); }, 1200  );

    setTimeout( function() { animeIn('seq01'); }, 3500 );

    setTimeout( function(){ show_flip(2, 0, 'textFlip', 800); }, 4500  );
    setTimeout( function(){ show_slide(2, 1, 'textSlideRight', 800); }, 4500  );
    setTimeout( function(){ show_flip(2, 2, 'textFlip', 800); }, 4900  );

    setTimeout( function() { animeIn('seq02'); }, 7000 );

    setTimeout( function(){ show_slide(3, 0, 'textFlip', 800); }, 7500  );
    setTimeout( function(){ show_slide(3, 1, 'textFlip', 800); }, 7700  );
    setTimeout( function(){ show_slide(3, 2, 'textSlideDown', 800); }, 7900  );

    setTimeout( function() { animeIn('seq03'); }, 8100 );


    if ( played < adRepeat ) {
        setTimeout( replay, 13000 );

    } else if (played >= adRepeat) {
  
        setTimeout( function() {
            // afficher replay ici
            replayBtn.animate({ opacity:'1' }, 250, 'ease-in-out', null, 0 );
            replayBtn.on('click', replay);
        }, 13000 );
    }
}

function init() {

    // Clear les inline style
    allDIV.attr('style', '');

    for (var i = 0; i < elements.length; i++) {
        elements.eq(i).removeClass('textFlip');
        elements.eq(i).animate({ rotateX: '-90deg', opacity: 1 }, 0 );
    }
}

function replay() {

    // Clear la derniere frame
    hide('logo', 0);
    hide('RolloverCTA', 0);
    hide('conteneurFrame3', 0);
    hide('conteneurFrame3_1', 0);
    hide('conteneurFrame3_2', 0);

    // Remove les class CSS
    removeClass();


    replayBtn.animate({ opacity:'0' }, 0, 'ease-in-out', null, 0 ); 
    played++;

    setTimeout( function(){
        init(); 
        animation();
    }, 500 );
}


$(function(){
    played = 1;
    elements = $('.element');
    allDIV = $('div');

    init();
    animation();

    $( ".RolloverCTA" ).mouseover(function() {
        $( ".arrow" ).animate({ left: '113px'}, 300, 'ease-out', null, 50 );
    });

    $( ".RolloverCTA" ).mouseout(function() {
        $( ".arrow" ).animate({ left: '110px'}, 300, 'ease-out', null, 50 );
    });
});