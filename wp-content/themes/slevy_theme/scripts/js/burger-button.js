
var burgerButton = {};

    burgerButton.body       = document.querySelector("body");
    burgerButton.masthead   = document.querySelector("#masthead");
    burgerButton.overlay    = document.querySelector(".overlay");
    burgerButton.btn        = document.querySelector(".c-hamburger");
    burgerButton.links      = document.querySelectorAll("nav li a");
    burgerButton.mainNav    = document.querySelector("nav");
    burgerButton.mobileBar  = document.querySelector(".mobile-bar");


 burgerButton.toggleHandler = function() {


        //hide button
        (burgerButton.btn.classList.contains("clicked") === true) ? burgerButton.btn.classList.remove("clicked") : burgerButton.btn.classList.add("clicked");

        //pointer events none on the mobile bar when clicked
        (burgerButton.mobileBar.classList.contains("no-pointer") === false) ? burgerButton.mobileBar.classList.add("no-pointer") : burgerButton.mobileBar.classList.remove("no-pointer");
        (burgerButton.masthead.classList.contains("no-pointer") === false) ? burgerButton.masthead.classList.add("no-pointer") : burgerButton.masthead.classList.remove("no-pointer");

        //put overlay black
        (burgerButton.overlay.classList.contains("clicked") === false) ? burgerButton.overlay.classList.add("clicked") : burgerButton.overlay.classList.remove("clicked");
        
        //freeze body
        (burgerButton.body.classList.contains("freeze") === false) ? burgerButton.body.classList.add("freeze") : burgerButton.body.classList.remove("freeze");

        //put links in white
        for (var i = 0; i < burgerButton.links.length; i++) {
            (burgerButton.links[i].classList.contains("clicked") === false) ? burgerButton.links[i].classList.add("clicked") : burgerButton.links[i].classList.remove("clicked");
        }
        
        //put nav visible
        setTimeout( function() {
            (burgerButton.mainNav.classList.contains("clicked") === false) ? burgerButton.mainNav.classList.add("clicked") : burgerButton.mainNav.classList.remove("clicked");
        }, 100 );
};



burgerButton.eventClick = function() {
    //when item is clicked : remove overlay and classe clicked
    burgerButton.toggleHandler();
};



burgerButton.eventListener = function() {
    burgerButton.btn.addEventListener( "click", function(e) {
        e.preventDefault();
        burgerButton.toggleHandler();
    });    

    burgerButton.overlay.addEventListener( "click", function(e) {
        e.preventDefault();
        burgerButton.toggleHandler();
    });

    if (window.innerWidth < 640) {
        for (var i = 0; i < burgerButton.links.length; i++) {
            burgerButton.links[i].addEventListener( "click", function(e) {
                burgerButton.toggleHandler();
                
            });
        }
    }
};


burgerButton.init = function() {
    burgerButton.eventListener();
};

burgerButton.init();