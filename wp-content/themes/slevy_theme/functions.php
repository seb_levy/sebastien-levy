<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'SEB_PORTFOLIO', 1.0 );






if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    // add_image_size('large', 960, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
}


/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'main-nav'	=>	__( 'Main nav Menu', 'slevy_theme' ), // Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu, 
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function seb_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar, 
		// just change the values of id and name to another word/name
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'seb_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function seb_scripts()  { 

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');
		
	// add theme scripts
	wp_enqueue_script( 'app', get_template_directory_uri() . '/scripts/app-min.js', array(), SEB_PORTFOLIO, true );

    // to handle ajax call in the script app.js
    // send 2 datas to js : ajaxurl and totalProjects
    wp_localize_script('app', 'appcall', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'totalProjects' => wp_count_posts('projects')->publish
    ));
  
}
add_action( 'wp_enqueue_scripts', 'seb_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header




/*-----------------------------------------------------------------------------------*/
/* Create custom post
/*-----------------------------------------------------------------------------------*/

function create_projects() {
    register_post_type('projects', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Works', 'projects'), // Rename these to suit
            'singular_name' => __('Projects', 'projects'),
            'add_new' => __('Add New Project', 'projects'),
            'add_new_item' => __('Add New Project', 'projects'),
            'edit' => __('Edit', 'projects'),
            'edit_item' => __('Edit Projects', 'projects'),
            'new_item' => __('New Projects', 'projects'),
            'view' => __('View Projects', 'projects'),
            'view_item' => __('View Projects', 'projects'),
            'search_items' => __('Search Projects', 'projects'),
            'not_found' => __('No Projectss found', 'projects'),
            'not_found_in_trash' => __('No Projects found in Trash', 'projects')
        ),
        'public' => true,
        'menu_position' => 4,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail',
            'custom-fields',
            'editor',


        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'category'
        ) // Add Category and Post Tags support
    ));
}
add_action('init', 'create_projects'); // Add our HTML5 Blank Custom Post Type




/*-----------------------------------------------------------------------------------*/
/* Create Meta Box
/*-----------------------------------------------------------------------------------*/

add_filter( 'rwmb_meta_boxes', 'projects_meta_boxes' );

function projects_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Project information', 'textdomain' ),
        'post_types' => 'projects',
        'fields'     => array(

            array(
                'id'   => 'description',
                'name' => __( 'Description', 'textdomain' ),
                'type' => 'textarea',
            ),

            // DIVIDER
            array(
                'type' => 'divider',
            ),

            array(
                'id'      => 'site-url',
                'name'    => __( 'Site Url', 'textdomain' ),
                'type'    => 'url',
            ),

            // DIVIDER
            array(
                'type' => 'divider',
            ),

            // UPLOAD LOGO
            array(
                'name'             => esc_html__( 'Upload logo', 'logo' ),
                'id'               => "logo",
                'type'             => 'plupload_image',
                'max_file_uploads' => 4,
            ),
            // UPLOAD IMAGES 
            array(
                'name'             => esc_html__( 'Upload images', 'project-images' ),
                'id'               => "project-images",
                'type'             => 'file_advanced',
            ),           
        ),
    );
    return $meta_boxes;
}


/*-----------------------------------------------------------------------------------*/
/* Ajax Call
/*-----------------------------------------------------------------------------------*/


add_action('wp_ajax_nopriv_load_more', 'load_more_projects');
add_action('wp_ajax_load_more', 'load_more_projects');

function load_more_projects() {

    //recupère la donnée offset
    $offset = $_POST['offset'];

    $args = array(
        'post_type' => 'projects',
        'offset' => $offset
        );

    $ajax_query = new WP_Query($args);
    // var_dump($ajax_query);

    if ( $ajax_query->have_posts() ): while ( $ajax_query->have_posts() ) : $ajax_query->the_post();
    
        get_template_part('display-post');
    
    endwhile;
    endif;

    die();
}