<?php
/**
 * 	Template Name: Landing
 *
 */

	get_header();
	$args = array(
				'post_type' => 'projects',
				'posts_per_page' => 6
			);
	$loop = new WP_query( $args);
?>

<main class="main">

	<div id="landing-page">

		<section class="title-container">
			<div class="the-title">
				<h4>French front&nbsp;end&nbsp;developer, graphic&nbsp;designer & photographer</h4>
				<h1>Sebastien  Levy</h1>
			</div>
			<div class="helper"></div>
		</section>
								
		<section class="the-content" id="works">
		<ul class="display-posts-listing">
			<?php 
				if ( $loop->have_posts() ):	while ($loop->have_posts() ): $loop->the_post();

					get_template_part('display-post');

				endwhile;
				endif;
				wp_reset_query();
			?>
		</ul>
	
		<div class="clear"></div>
		<div class="load-more"></div>
		
		</section>
		<section class="about-me" id="about">

			<?php 
				while( have_posts() ) : the_post();
				
					echo the_content();
				
				endwhile;
				wp_reset_query();
			?>
			
		</section>
	</div>
<?php get_footer(); ?>