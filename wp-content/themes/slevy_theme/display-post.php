<li class='listing-item'>
	<a class='image' href='<?php the_permalink() ?>'>
		<img width='960' height='540' class='tilt-effect' src='<?php the_post_thumbnail_url() ?>'/>
	</a>
</li>