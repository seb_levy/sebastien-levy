<?php get_header('single'); ?>

<main class="main single"><!-- start the page containter -->

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <section class="single-project">

        <!-- article -->
        <article>

            <div class="header-content">

                <div class="title-project">
                    <h3><?php echo the_title(); ?></h3>
                </div>

                <a class="nav-back" href="<?php bloginfo('url'); ?>/#works">Back to works</a>

                <div class="description-project">
                    <p><?php echo rwmb_meta( 'description'); ?></p>
                </div>
                <div class="center">
                    <?php 
                        $link = rwmb_meta( 'site-url' );
                        if ( !empty( $link ) ) {
                            echo "<a href=".$link." class='website-link' target='_blank'>Visit the website</a><div class='clear'></div>";
                        }
                    ?>
                </div>
            </div>

            <div class="logo-project">
            <?php 
                $images = rwmb_meta( 'logo', 'size=auto' );
                if ( !empty( $images ) ) {
                    foreach ( $images as $image ) {
                        echo "<img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />";
                    }
                }
            ?>
            </div>

            <div class="images-project">
                <?php 
                    $images = rwmb_meta( 'project-images', 'size=auto' );
                    if ( !empty( $images ) ) {
                        foreach ( $images as $image ) {
                            echo "<img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />";
                        }
                    }
                ?>
            </div>

            <div class="banners">
                <?php 
                    $content = get_the_content();
                    if ( !empty( $content ) ) {
                        echo the_content(); 
                    }
                ?>
            </div>

        </article>
        <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>

                <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

            </article>
            <!-- /article -->

        <?php endif; ?>


    </section>


<!-- /section -->


<?php get_footer(); ?>