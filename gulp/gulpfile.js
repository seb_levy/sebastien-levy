/*!
 * gulp
 * $ npm install gulp-less gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    less = require('gulp-less'),
    cssbeautify = require('gulp-cssbeautify'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload');

// Variables de chemins
var source = '../wp-content/themes/slevy_theme/'; // dossier de travail
var destination = '../wp-content/themes/slevy_theme/'; // dossier à livrer

// Styles
gulp.task('css', function() {
  return gulp.src(source + 'styles/app.less')
    .pipe(less())
    .pipe(cssbeautify({indent: '  '}))
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest(destination + 'styles/'))
    .pipe(livereload())
    .pipe(notify({ message: 'Styles task complete' }));
});

// Scripts
gulp.task('js', function() {
  return gulp.src(source + 'scripts/js/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(rename({ suffix: '-min' }))
    .pipe(uglify())
    .pipe(gulp.dest(destination + 'scripts/'))
    .pipe(livereload())
    .pipe(notify({ message: 'Styles task complete' }));
});

// Images
// gulp.task('images', function() {
//   return gulp.src('src/images/**/*')
//     .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//     .pipe(gulp.dest('dist/images'))
//     .pipe(notify({ message: 'Images task complete' }));
// });


// Tâche Minify Scripts
gulp.task('concatscripts', function() {
  return gulp.src(source + 'scripts/js/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(rename({ suffix: '-min' }))
    .pipe(uglify())
    .pipe(gulp.dest(destination + 'scripts/'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Tâche "minify" = minification CSS (destination -> destination)
gulp.task('minifycss', function () {
  return gulp.src(destination + '/styles/*.css')
    .pipe(cssnano())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(destination + '/styles/'));
});


// Tâche "build"
gulp.task('buildcss', ['css']);
gulp.task('buildjs', ['js']);


// Tâche "prod" = Build + minify
gulp.task('prod', ['build',  'minifycss',  'concatscripts']);


// Tâche par défaut
gulp.task('default', ['buildcss', 'buildjs']);

// Tâche "watch" = je surveille *less
gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(source + '/styles/less/*.less', ['buildcss']);
  gulp.watch(source + '/scripts/js/*.js', ['buildjs']);
});